# Nextjs on Firebase

blog: https://twinsynergy.co.th/deploying-next-js-app-to-firebase/

## แก้ไข firebase project id

ทำการแก้ไข `<your_firebase_project_id>` ใน `.firebaserc`

## ทดสอบสั่ง Deploy

```bash
npm install
npm run predeploy
npm run deploy
```
